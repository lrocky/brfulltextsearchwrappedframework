#
# Be sure to run `pod lib lint BRFullTextSearch.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BRFullTextSearch'
  s.version          = '0.1.0'
  s.summary          = 'Wrapper of compiled library by Blue-Rocket/BRFullTextSearch'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This is a wrapper for BRFullTextSearch.framework, located https://github.com/mikhailmulyar/BRFullTextSearch
                       DESC

  s.homepage         = 'https://github.com/timur-dinmukhametov/BRFullTextSearch'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'timur-dinmukhametov' => 't.dinmukhametov@immergion.ltd' }
  s.source           = { :git => 'https://github.com/timur-dinmukhametov/BRFullTextSearch.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'BRFullTextSearch/Classes/**/*'
  s.vendored_frameworks = 'BRFullTextSearch.framework'

  # s.resource_bundles = {
  #   'BRFullTextSearch' => ['BRFullTextSearch/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
