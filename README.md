# BRFullTextSearch

[![CI Status](https://img.shields.io/travis/timur-dinmukhametov/BRFullTextSearch.svg?style=flat)](https://travis-ci.org/timur-dinmukhametov/BRFullTextSearch)
[![Version](https://img.shields.io/cocoapods/v/BRFullTextSearch.svg?style=flat)](https://cocoapods.org/pods/BRFullTextSearch)
[![License](https://img.shields.io/cocoapods/l/BRFullTextSearch.svg?style=flat)](https://cocoapods.org/pods/BRFullTextSearch)
[![Platform](https://img.shields.io/cocoapods/p/BRFullTextSearch.svg?style=flat)](https://cocoapods.org/pods/BRFullTextSearch)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BRFullTextSearch is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BRFullTextSearch', :git => 'https://bitbucket.org/lrocky/brfulltextsearchwrappedframework'
```

## Author

timur-dinmukhametov, t.dinmukhametov@immergion.ltd

## License

BRFullTextSearch is available under the MIT license. See the LICENSE file for more info.
